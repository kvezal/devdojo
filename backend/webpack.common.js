const path = require('path');

module.exports = {
  entry: './src/main.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader'
      }
    ]
  },
  resolve: {extensions: ['.tsx', '.ts', '.js']},
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    globalObject: 'global',
    clean: true
  },
  optimization: {
    concatenateModules: false,
    splitChunks: {chunks: 'all'}
  },
};