import { CommentInterface } from './comment.interface';
import { UserInterface } from './user.interface';

export interface CommentParamsInterface {
  comment: CommentInterface;
  user: UserInterface;
}