import { CommentInterface } from './comment.interface';
import { List } from '../core';
import { UserInterface } from './user.interface';

export interface CommentListParamsInterface {
  commentList: List<CommentInterface>;
  users: UserInterface[];
}