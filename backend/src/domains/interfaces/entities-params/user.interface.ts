export interface UserInterface {
  id: string;
  name: string;
  image: string;
  rating: number;
}