import { ContentItemTypeEnum } from '../../enums/content-item-type.enum';

export interface ContentItemParamsInterface {
  type: ContentItemTypeEnum;
  text: string;
  weight: string;
  size: string;
  color: string;
}