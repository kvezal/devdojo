export { ArticleInterface } from './article.interface';
export { ArticleParamsInterface } from './article-params.interface';
export { CommentInterface } from './comment.interface';
export { CommentListParamsInterface } from './comment-list-params.interface';
export { CommentParamsInterface } from './comment-params.interface';
export { ContentItemParamsInterface } from './content-item-params.interface';
export { CourseInterface } from './course.interface';
export { UserInterface } from './user.interface';