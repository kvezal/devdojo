import { ArticleInterface } from './article.interface';
import { UserInterface } from './user.interface';

export interface ArticleParamsInterface {
  article: ArticleInterface;
  author: UserInterface;
}