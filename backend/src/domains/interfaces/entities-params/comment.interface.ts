export interface CommentInterface {
  id: string;
  articleId: string;
  authorId: string;
  text: string;
  date: string;
}