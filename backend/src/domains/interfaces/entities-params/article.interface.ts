import { ContentItemParamsInterface } from './content-item-params.interface';

export interface ArticleInterface {
  id: string;
  name: string;
  image: string;
  category: string;
  contentItems: ContentItemParamsInterface[];
  authorId: string;
  courseId: string;
}