export interface CourseInterface {
  id: string;
  name: string;
  text: string;
  image: string;
}