export interface Pagination {
  pageItemQuantity: number;
  page: number;
}