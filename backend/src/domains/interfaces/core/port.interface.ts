export interface PortInterface<P, R> {
  connect(params: P): Promise<R>;
}