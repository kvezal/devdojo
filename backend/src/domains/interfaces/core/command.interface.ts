export interface CommandInterface<P, R> {
  run(param: P): Promise<R>;
}