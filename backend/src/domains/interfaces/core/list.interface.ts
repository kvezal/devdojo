import { Pagination } from './pagination.interface';

export interface List<T> extends Pagination {
  items: T[];
  totalQuantity: number;
}