import { Pagination } from '../core/pagination.interface';

export interface ReadArticleInfoListParamsInterface extends Pagination {
  courseId: string;
}