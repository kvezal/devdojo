import { Pagination } from '../core';

export interface ReadArticleCommentListParamsInterface extends Pagination {
  articleId: string;
}