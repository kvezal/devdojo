export interface CreateCourseParamsInterface {
  name: string;
  text: string;
  image: string;
}