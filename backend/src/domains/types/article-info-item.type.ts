import { ArticleInterface } from '../interfaces/entities-params/article.interface';

export type ArticleInfoItemType = Pick<ArticleInterface, 'id' | 'name' | 'image'>;