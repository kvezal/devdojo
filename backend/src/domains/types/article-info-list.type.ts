import { ArticleInfoItemType } from './article-info-item.type';
import { List } from '../interfaces/core';

export type ArticleInfoListType = List<ArticleInfoItemType>;