import { CourseInterface } from '../interfaces/entities-params';
import { List } from '../interfaces/core';

export type CourseListType = List<CourseInterface>;