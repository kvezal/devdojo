import { ReadArticleInfoListCommand } from './read-article-info-list.command';
import { ReadArticleInfoListPort } from '../../../ports';
import { ReadArticleInfoListParamsInterface } from '../../../interfaces/port-params';
import { ArticleInfoListEntity } from '../../../entities/article';
import { ARTICLE_INFO_LIST_PARAMS_MOCK } from '../../../mocks/article/article-info-list-params.mock';

describe(ReadArticleInfoListCommand, () => {
  let command: ReadArticleInfoListCommand;
  let readArticleInfoListPort: ReadArticleInfoListPort;

  beforeEach(() => {
    readArticleInfoListPort = { connect: () => Promise.resolve(null) };
    command = new ReadArticleInfoListCommand(readArticleInfoListPort);
  });

  it('should be created', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of ReadArticleInfoListPort', async() => {
      const connectSpy = jest.spyOn(readArticleInfoListPort, 'connect')
        .mockResolvedValueOnce(ARTICLE_INFO_LIST_PARAMS_MOCK);
      const loadArticleInfoListParams: ReadArticleInfoListParamsInterface = {
        courseId: '33d98b84-f9d8-4575-905b-9431cfc0165e',
        pageItemQuantity: 10,
        page: 1
      };
      await command.run(loadArticleInfoListParams);
      expect(connectSpy).toHaveBeenCalledWith(loadArticleInfoListParams);
    })

    it('should be called create method of ArticleInfoListEntity', async() => {
      const createSpy = jest.spyOn(ArticleInfoListEntity, 'create')
        .mockReturnValueOnce(null);
      jest.spyOn(readArticleInfoListPort, 'connect')
        .mockResolvedValueOnce(ARTICLE_INFO_LIST_PARAMS_MOCK)
      const loadArticleInfoListParams: ReadArticleInfoListParamsInterface = {
        courseId: '33d98b84-f9d8-4575-905b-9431cfc0165e',
        pageItemQuantity: 10,
        page: 1
      };
      await command.run(loadArticleInfoListParams);
      expect(createSpy).toHaveBeenCalledWith(ARTICLE_INFO_LIST_PARAMS_MOCK);
    })

    it('should return result of ArticleInfoListEntity\'s create method', async() => {
      jest.spyOn(readArticleInfoListPort, 'connect')
        .mockResolvedValueOnce(ARTICLE_INFO_LIST_PARAMS_MOCK);
      const expectedResult = new ArticleInfoListEntity(ARTICLE_INFO_LIST_PARAMS_MOCK);
      jest.spyOn(ArticleInfoListEntity, 'create')
        .mockReturnValueOnce(expectedResult);
      const loadArticleInfoListParams: ReadArticleInfoListParamsInterface = {
        courseId: '33d98b84-f9d8-4575-905b-9431cfc0165e',
        pageItemQuantity: 10,
        page: 1
      };
      const result = await command.run(loadArticleInfoListParams);
      expect(result).toEqual(expectedResult);
    })
  });
});