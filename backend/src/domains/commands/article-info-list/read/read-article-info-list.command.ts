import { ArticleInfoListEntity } from '../../../entities/article';
import { ArticleInfoListType } from '../../../types';
import { CommandInterface } from '../../../interfaces/core';
import { ReadArticleInfoListParamsInterface } from '../../../interfaces/port-params';
import { ReadArticleInfoListPort } from '../../../ports';

export class ReadArticleInfoListCommand implements
  CommandInterface<ReadArticleInfoListParamsInterface, ArticleInfoListEntity> {
  constructor(private readonly readArticleInfoListPort: ReadArticleInfoListPort) {}

  public async run(params: ReadArticleInfoListParamsInterface): Promise<ArticleInfoListEntity> {
    const articleInfoListParams: ArticleInfoListType = await this.readArticleInfoListPort.connect(params);
    return ArticleInfoListEntity.create(articleInfoListParams);
  }
}