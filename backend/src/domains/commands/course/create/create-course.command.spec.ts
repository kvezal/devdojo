import { CreateCourseCommand } from './create-course.command';
import { CreateCoursePort } from '../../../ports';
import { COURSE_MOCK, CREATE_COURSE_PARAMS_MOCK } from '../../../mocks';
import { CourseItemEntity } from '../../../entities/course';

describe(CreateCourseCommand, () => {
  let command: CreateCourseCommand;

  let createCoursePort: CreateCoursePort;

  beforeEach(() => {
    createCoursePort = { connect: () => Promise.resolve(null) };
    command = new CreateCourseCommand(createCoursePort);
  });

  it('should be defined', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of readCoursePort', async () => {
      const connectSpy = jest.spyOn(createCoursePort, 'connect')
        .mockResolvedValueOnce(COURSE_MOCK);
      await command.run(CREATE_COURSE_PARAMS_MOCK);
      expect(connectSpy).toHaveBeenCalledWith(CREATE_COURSE_PARAMS_MOCK);
    });

    it('should be called create method of CourseEntity', async () => {
      const createSpy = jest.spyOn(CourseItemEntity, 'create');
      jest.spyOn(createCoursePort, 'connect')
        .mockResolvedValueOnce(COURSE_MOCK);
      await command.run(CREATE_COURSE_PARAMS_MOCK);
      expect(createSpy).toHaveBeenCalledWith(COURSE_MOCK);
    });

    it('should return result of CourseEntity\'s create method', async () => {
      jest.spyOn(createCoursePort, 'connect')
        .mockResolvedValueOnce(COURSE_MOCK);
      const expectedResult = CourseItemEntity.create(COURSE_MOCK);
      jest.spyOn(CourseItemEntity, 'create')
        .mockReturnValueOnce(expectedResult);
      const result = await command.run(CREATE_COURSE_PARAMS_MOCK);
      expect(result).toEqual(expectedResult);
    });
  });
});