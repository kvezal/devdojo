import { CommandInterface } from '../../../interfaces/core';
import { CourseInterface } from '../../../interfaces/entities-params';
import { CourseItemEntity } from '../../../entities/course';
import { CreateCourseParamsInterface } from '../../../interfaces/port-params';
import { CreateCoursePort } from '../../../ports';

export class CreateCourseCommand implements CommandInterface<CreateCourseParamsInterface, CourseItemEntity> {
  constructor(private readonly createCoursePort: CreateCoursePort) {}

  public async run(params: CreateCourseParamsInterface): Promise<CourseItemEntity> {
    const course: CourseInterface = await this.createCoursePort.connect(params);
    return CourseItemEntity.create(course);
  }
}