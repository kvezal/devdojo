import { COURSE_MOCK } from '../../../mocks';
import { CourseItemEntity } from '../../../entities/course';
import { ReadCoursePort } from '../../../ports';
import { ReadCourseCommand } from './read-course.command';

describe(ReadCourseCommand, () => {
  let command: ReadCourseCommand;
  let readCoursePort: ReadCoursePort;

  beforeEach(() => {
    readCoursePort = { connect: () => Promise.resolve(null) };
    command = new ReadCourseCommand(readCoursePort);
  });

  it('should be defined', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of readCoursePort', async () => {
      const connectSpy = jest.spyOn(readCoursePort, 'connect')
        .mockResolvedValueOnce(COURSE_MOCK);
      command.run(COURSE_MOCK.id);
      expect(connectSpy).toHaveBeenCalledWith(COURSE_MOCK.id);
    });

    it('should be called create method of CourseEntity', async () => {
      const createSpy = jest.spyOn(CourseItemEntity, 'create');
      jest.spyOn(readCoursePort, 'connect')
        .mockResolvedValueOnce(COURSE_MOCK);
      await command.run(COURSE_MOCK.id);
      expect(createSpy).toHaveBeenCalledWith(COURSE_MOCK);
    });

    it('should return result of CourseEntity\'s create method', async () => {
      jest.spyOn(readCoursePort, 'connect')
        .mockResolvedValueOnce(COURSE_MOCK);
      const expectedResult = CourseItemEntity.create(COURSE_MOCK);
      jest.spyOn(CourseItemEntity, 'create')
        .mockReturnValueOnce(expectedResult);
      const result = await command.run(COURSE_MOCK.id);
      expect(result).toEqual(expectedResult);
    });
  });
});