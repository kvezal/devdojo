import { CommandInterface } from '../../../interfaces/core';
import { CourseItemEntity } from '../../../entities/course';
import { ReadCoursePort } from '../../../ports';

export class ReadCourseCommand implements CommandInterface<string, CourseItemEntity> {
  constructor(private readonly readCoursePort: ReadCoursePort) {}

  public async run(courseId: string): Promise<CourseItemEntity> {
    const course = await this.readCoursePort.connect(courseId);
    return CourseItemEntity.create(course);
  }
}