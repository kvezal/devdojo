import { ReadArticleCommentListCommand } from './read-article-comment-list.command';
import { ReadArticleCommentListPort, ReadUsersByIdsPort } from '../../../ports';
import { COMMENT_LIST_PARAMS_MOCK, USERS_MOCK } from '../../../mocks';
import { CommentListEntity } from '../../../entities/comment';

describe(ReadArticleCommentListCommand, () => {
  let command: ReadArticleCommentListCommand;
  let readArticleCommentListPort: ReadArticleCommentListPort;
  let readUsersByIdsPort: ReadUsersByIdsPort;

  beforeEach(() => {
    readArticleCommentListPort = { connect: () => Promise.resolve(null) };
    readUsersByIdsPort = { connect: () => Promise.resolve(null) };
    command = new ReadArticleCommentListCommand(
      readArticleCommentListPort,
      readUsersByIdsPort,
    );
  })

  it('should be created', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of ReadArticleCommentListPort', async () => {
      const connectSpy = jest.spyOn(readArticleCommentListPort, 'connect')
        .mockResolvedValueOnce(COMMENT_LIST_PARAMS_MOCK);
      const params = {
        articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
        pageItemQuantity: 10,
        page: 1,
      };
      await command.run(params);
      expect(connectSpy).toHaveBeenCalledWith(params)
    });

    it('should be called connect method of ReadUsersByIdsPort', async () => {
      const connectSpy = jest.spyOn(readUsersByIdsPort, 'connect');
      jest.spyOn(readArticleCommentListPort, 'connect')
        .mockResolvedValueOnce(COMMENT_LIST_PARAMS_MOCK);
      await command.run({
        articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
        pageItemQuantity: 10,
        page: 1,
      });
      expect(connectSpy).toHaveBeenCalledWith([
        USERS_MOCK[0].id,
        USERS_MOCK[1].id,
      ])
    });

    it('should be called create method of CommentListEntity', async () => {
      const createSpy = jest.spyOn(CommentListEntity, 'create').mockReturnValueOnce(null);
      jest.spyOn(readArticleCommentListPort, 'connect')
        .mockResolvedValueOnce(COMMENT_LIST_PARAMS_MOCK);
      jest.spyOn(readUsersByIdsPort, 'connect')
        .mockResolvedValueOnce(USERS_MOCK);
      await command.run({
        articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
        pageItemQuantity: 10,
        page: 1,
      });
      expect(createSpy).toHaveBeenCalledWith({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK,
      })
    });

    it('should return correct result', async () => {
      jest.spyOn(readArticleCommentListPort, 'connect')
        .mockResolvedValueOnce(COMMENT_LIST_PARAMS_MOCK);
      jest.spyOn(readUsersByIdsPort, 'connect')
        .mockResolvedValueOnce(USERS_MOCK);

      const expectedResult = new CommentListEntity({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK,
      });
      jest.spyOn(CommentListEntity, 'create').mockReturnValueOnce(expectedResult);

      const result = await command.run({
        articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
        pageItemQuantity: 10,
        page: 1,
      });
      expect(result).toEqual(expectedResult);
    });
  });
});