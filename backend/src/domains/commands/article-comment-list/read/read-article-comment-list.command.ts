import { CommandInterface, List } from '../../../interfaces/core';
import { CommentInterface, UserInterface } from '../../../interfaces/entities-params';
import { ReadArticleCommentListPort, ReadUsersByIdsPort } from '../../../ports';
import { ArrayConverterUtils } from '../../../utils';
import { CommentListEntity } from '../../../entities/comment';
import { ReadArticleCommentListParamsInterface } from '../../../interfaces/port-params';

export class ReadArticleCommentListCommand implements
  CommandInterface<ReadArticleCommentListParamsInterface, CommentListEntity> {
  constructor(
    private readonly readArticleCommentListPort: ReadArticleCommentListPort,
    private readonly readUsersByIdsPort: ReadUsersByIdsPort
  ) {}

  public async run(param: ReadArticleCommentListParamsInterface): Promise<CommentListEntity> {
    const commentList: List<CommentInterface> = await this.readArticleCommentListPort.connect(param);
    const userIdSet = ArrayConverterUtils.convertToSetByProperty({
      items: commentList.items,
      property: 'authorId'
    });
    const users: UserInterface[] = await this.readUsersByIdsPort.connect([...userIdSet]);
    return CommentListEntity.create({
      commentList,
      users
    });
  }
}