import { ArticleInterface, UserInterface } from '../../../interfaces/entities-params';
import { ReadUserByIdPort, UpdateArticlePort } from '../../../ports';
import { ArticleItemEntity } from '../../../entities/article';
import { CommandInterface } from '../../../interfaces/core';

export class UpdateArticleCommand implements CommandInterface<ArticleInterface, ArticleItemEntity> {
  constructor(
    private readonly readUserByIdPort: ReadUserByIdPort,
    private readonly updateArticlePort: UpdateArticlePort
  ) {}

  public async run(article: ArticleInterface): Promise<ArticleItemEntity> {
    const author: UserInterface = await this.readUserByIdPort.connect(article.authorId);
    const articleEntity = ArticleItemEntity.create({ article, author });
    await this.updateArticlePort.connect(articleEntity);
    return articleEntity;
  }
}