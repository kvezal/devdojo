import { ARTICLE_MOCK, USER_MOCK } from '../../../mocks';
import { ReadUserByIdPort, UpdateArticlePort } from '../../../ports';
import { UpdateArticleCommand } from './update-article.command';
import { ArticleItemEntity } from '../../../entities/article';

describe(UpdateArticleCommand, () => {
  let command: UpdateArticleCommand;
  let readUserByIdPort: ReadUserByIdPort;
  let updateArticlePort: UpdateArticlePort;

  beforeEach(() => {
    readUserByIdPort = { connect: () => Promise.resolve(null) };
    updateArticlePort = { connect: () => Promise.resolve(null) };
    command = new UpdateArticleCommand(
      readUserByIdPort,
      updateArticlePort
    );
  });

  it('should be definer', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of ReadUserByIdPort', async () => {
      const connectSpy = jest.spyOn(readUserByIdPort, 'connect');
      await command.run(ARTICLE_MOCK);
      expect(connectSpy).toHaveBeenCalledWith(ARTICLE_MOCK.authorId);
    });

    it('should be called create method', async () => {
      const createSpy = jest.spyOn(ArticleItemEntity, 'create');
      jest.spyOn(readUserByIdPort, 'connect')
        .mockResolvedValueOnce(USER_MOCK);
      await command.run(ARTICLE_MOCK);
      expect(createSpy).toHaveBeenCalledWith({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
    });

    it('should be called connect method of UpdateArticlePort', async () => {
      const connectSpy = jest.spyOn(updateArticlePort, 'connect');

      jest.spyOn(readUserByIdPort, 'connect');

      const articleEntity = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      jest.spyOn(ArticleItemEntity, 'create')
        .mockReturnValueOnce(articleEntity);
      await command.run(ARTICLE_MOCK);
      expect(connectSpy).toHaveBeenCalledWith(articleEntity);
    });

    it('should return result of ArticleEntity\'s create method', async () => {
      jest.spyOn(readUserByIdPort, 'connect');

      const articleEntity = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      jest.spyOn(ArticleItemEntity, 'create')
        .mockReturnValueOnce(articleEntity);
      jest.spyOn(updateArticlePort, 'connect');
      const result = await command.run(ARTICLE_MOCK);
      expect(result).toEqual(articleEntity);
    });
  });
});