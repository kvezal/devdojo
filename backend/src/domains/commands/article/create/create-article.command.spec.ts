import { ARTICLE_MOCK, USER_MOCK } from '../../../mocks';
import { CreateArticlePort, ReadUserByIdPort } from '../../../ports';
import { CreateArticleCommand } from './create-article.command';
import { ArticleItemEntity } from '../../../entities/article';

describe(CreateArticleCommand, () => {
  let command: CreateArticleCommand;
  let readUserByIdPort: ReadUserByIdPort;
  let createArticlePort: CreateArticlePort;

  beforeEach(() => {
    readUserByIdPort = { connect: () => Promise.resolve(null) };
    createArticlePort = { connect: () => Promise.resolve(null) };
    command = new CreateArticleCommand(
      readUserByIdPort,
      createArticlePort
    );
  });

  it('should be definer', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of ReadUserByIdPort', async () => {
      const connectSpy = jest.spyOn(readUserByIdPort, 'connect');
      await command.run(ARTICLE_MOCK);
      expect(connectSpy).toHaveBeenCalledWith(ARTICLE_MOCK.authorId);
    });

    it('should be called create method', async () => {
      const createSpy = jest.spyOn(ArticleItemEntity, 'create');
      jest.spyOn(readUserByIdPort, 'connect')
        .mockResolvedValueOnce(USER_MOCK);
      await command.run(ARTICLE_MOCK);
      expect(createSpy).toHaveBeenCalledWith({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
    });

    it('should be called connect method of CreateArticlePort', async () => {
      const connectSpy = jest.spyOn(createArticlePort, 'connect');

      jest.spyOn(readUserByIdPort, 'connect');

      const articleEntity = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      jest.spyOn(ArticleItemEntity, 'create')
        .mockReturnValueOnce(articleEntity);
      await command.run(ARTICLE_MOCK);
      expect(connectSpy).toHaveBeenCalledWith(articleEntity);
    });

    it('should be called connect method of CreateArticlePort', async () => {
      jest.spyOn(readUserByIdPort, 'connect');

      const articleEntity = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      jest.spyOn(ArticleItemEntity, 'create')
        .mockReturnValueOnce(articleEntity);
      jest.spyOn(createArticlePort, 'connect');
      const result = await command.run(ARTICLE_MOCK);
      expect(result).toEqual(articleEntity);
    });
  });
});