import { ArticleInterface, UserInterface } from '../../../interfaces/entities-params';
import { CreateArticlePort, ReadUserByIdPort } from '../../../ports';
import { ArticleItemEntity } from '../../../entities/article';
import { CommandInterface } from '../../../interfaces/core';

export class CreateArticleCommand implements CommandInterface<ArticleInterface, ArticleItemEntity> {
  constructor(
    private readonly readUserByIdPort: ReadUserByIdPort,
    private readonly createArticlePort: CreateArticlePort
  ) {}

  public async run(article: ArticleInterface): Promise<ArticleItemEntity> {
    const author: UserInterface = await this.readUserByIdPort.connect(article.authorId);
    const articleEntity = ArticleItemEntity.create({ article, author });
    await this.createArticlePort.connect(articleEntity);
    return articleEntity;
  }
}