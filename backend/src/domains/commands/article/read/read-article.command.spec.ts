import { ARTICLE_MOCK, USER_MOCK } from '../../../mocks';
import { ReadArticlePort, ReadUserByIdPort } from '../../../ports';
import { ArticleItemEntity } from '../../../entities/article';
import { ReadArticleCommand } from './read-article.command';

describe(ReadArticleCommand, () => {
  let command: ReadArticleCommand;
  let readArticlePort: ReadArticlePort;
  let readUserByIdPort: ReadUserByIdPort;

  beforeEach(() => {
    readArticlePort = {  connect: () => Promise.resolve(null) };
    readUserByIdPort = {  connect: () => Promise.resolve(null) };
    command = new ReadArticleCommand(
      readArticlePort,
      readUserByIdPort,
    );
  })

  it('should be created', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of ReadArticlePort', async () => {
      const connectSpy = jest.spyOn(readArticlePort, 'connect')
        .mockResolvedValueOnce(ARTICLE_MOCK);
      await command.run(ARTICLE_MOCK.id)
      expect(connectSpy).toHaveBeenCalledWith(ARTICLE_MOCK.id);
    });

    it('should be called connect method of ReadUserByIdPort', async () => {
      const connectSpy = jest.spyOn(readUserByIdPort, 'connect');
      jest.spyOn(readArticlePort, 'connect')
        .mockResolvedValueOnce(ARTICLE_MOCK);
      await command.run(ARTICLE_MOCK.id)
      expect(connectSpy).toHaveBeenCalledWith(ARTICLE_MOCK.authorId);
    });

    it('should be called create method of ArticleEntity', async () => {
      const createSpy = jest.spyOn(ArticleItemEntity, 'create');
      jest.spyOn(readArticlePort, 'connect')
        .mockResolvedValueOnce(ARTICLE_MOCK);
      jest.spyOn(readUserByIdPort, 'connect')
        .mockResolvedValueOnce(USER_MOCK);
      await command.run(ARTICLE_MOCK.id)
      expect(createSpy).toHaveBeenCalledWith({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
    });

    it('should return result of create method of ArticleEntity', async () => {
      jest.spyOn(readArticlePort, 'connect')
        .mockResolvedValueOnce(ARTICLE_MOCK);
      jest.spyOn(readUserByIdPort, 'connect')
        .mockResolvedValueOnce(USER_MOCK);
      const expectedResult = new ArticleItemEntity({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      jest.spyOn(ArticleItemEntity, 'create')
        .mockReturnValueOnce(expectedResult)
      const result = await command.run(ARTICLE_MOCK.id)
      expect(result).toEqual(expectedResult);
    });
  });
});