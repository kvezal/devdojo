import { ArticleInterface, UserInterface } from '../../../interfaces/entities-params';
import { ReadArticlePort, ReadUserByIdPort } from '../../../ports';
import { ArticleItemEntity } from '../../../entities/article';
import { CommandInterface } from '../../../interfaces/core';

export class ReadArticleCommand implements CommandInterface<string, ArticleItemEntity> {
  constructor(
    private readonly readArticlePort: ReadArticlePort,
    private readonly readUserByIdPort: ReadUserByIdPort
  ) {}

  public async run(articleId: string): Promise<ArticleItemEntity> {
    const article: ArticleInterface = await this.readArticlePort.connect(articleId);
    const author: UserInterface = await this.readUserByIdPort.connect(article.authorId);
    return ArticleItemEntity.create({
      article,
      author
    });
  }
}