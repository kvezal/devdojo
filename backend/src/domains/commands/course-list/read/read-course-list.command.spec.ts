import { CourseListEntity } from '../../../entities/course';
import { ReadCourseListCommand } from './read-course-list.command';
import { ReadCourseListPort } from '../../../ports';
import { COURSE_LIST_MOCK } from '../../../mocks';

describe(ReadCourseListCommand, () => {
  let command: ReadCourseListCommand;
  let readCourseListPort: ReadCourseListPort;

  beforeEach(() => {
    readCourseListPort = { connect: () => Promise.resolve(null) };
    command = new ReadCourseListCommand(readCourseListPort);
  });

  it('should be created', () => {
    expect(command).toBeDefined();
  });

  describe('run method', () => {
    it('should be called connect method of ReadCourseListPort', async () => {
      const connectSpy = jest.spyOn(readCourseListPort, 'connect')
        .mockResolvedValueOnce(COURSE_LIST_MOCK);
      const params = {
        pageItemQuantity: 10,
        page: 6,
      };
      await command.run(params);
      expect(connectSpy).toHaveBeenCalledWith(params);
    });

    it('should be called create method of CourseListEntity', async () => {
      const createSpy = jest.spyOn(CourseListEntity, 'create');
      jest.spyOn(readCourseListPort, 'connect')
        .mockResolvedValueOnce(COURSE_LIST_MOCK);
      await command.run({
        pageItemQuantity: 10,
        page: 6,
      });
      expect(createSpy).toHaveBeenCalledWith(COURSE_LIST_MOCK);
    });

    it('should return result of create method of CourseListEntity', async () => {
      jest.spyOn(readCourseListPort, 'connect')
        .mockResolvedValueOnce(COURSE_LIST_MOCK);
      const expectedResult = CourseListEntity.create(COURSE_LIST_MOCK);
      jest.spyOn(CourseListEntity, 'create')
        .mockReturnValueOnce(expectedResult);
      const result = await command.run({
        pageItemQuantity: 10,
        page: 6,
      });
      expect(result).toEqual(expectedResult);
    });
  });
});