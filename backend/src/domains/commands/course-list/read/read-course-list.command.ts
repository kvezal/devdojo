import { CommandInterface, Pagination } from '../../../interfaces/core';
import { CourseListEntity } from '../../../entities/course';
import { CourseListType } from '../../../types';
import { ReadCourseListPort } from '../../../ports';

export class ReadCourseListCommand implements CommandInterface<Pagination, CourseListEntity> {
  constructor(private readonly readCourseListPort: ReadCourseListPort) {}

  public async run(param: Pagination): Promise<CourseListEntity> {
    const courseList: CourseListType = await this.readCourseListPort.connect(param);
    return CourseListEntity.create(courseList);
  }
}