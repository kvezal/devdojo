import { ArrayConverterUtils } from './array-converter.utils';

describe(ArrayConverterUtils, () => {
  describe('convertToMapByProperty method', () => {
    it('should return correct result', () => {
      const result = ArrayConverterUtils.convertToMapByProperty({
        items: [
          { id: 1, value: '1' },
          { id: 2, value: '2' },
          { id: 1, value: '3' },
        ],
        property: 'id',
      });
      expect(result).toEqual(new Map([
        [1, { id: 1, value: '3' }],
        [2, { id: 2, value: '2' }],
      ]));
    });
  });

  describe('convertToMapByProperty method', () => {
    it('should return correct result', () => {
      const result = ArrayConverterUtils.convertToSetByProperty({
        items: [
          { id: 1, value: '1' },
          { id: 2, value: '2' },
          { id: 1, value: '3' },
        ],
        property: 'id',
      });
      expect(result).toEqual(new Set([1, 2]));
    });
  });
});