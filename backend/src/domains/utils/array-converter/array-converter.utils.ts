export class ArrayConverterUtils {
  static convertToMapByProperty<Item, Key extends keyof Item>(params: {
    items: Item[];
    property: Key;
  }): Map<Item[Key], Item> {
    return (params.items ?? []).reduce(
      (result: Map<Item[Key], Item>, item: Item) => result.set(item[params.property], item), new Map()
    );
  }

  static convertToSetByProperty<Item, Key extends keyof Item>(params: {
    items: Item[];
    property: Key;
  }): Set<Item[Key]> {
    return (params.items ?? []).reduce(
      (result: Set<Item[Key]>, item: Item) => result.add(item[params.property]), new Set()
    );
  }
}