import { List, PortInterface } from '../../interfaces/core';
import { CommentInterface } from '../../interfaces/entities-params';
import { ReadArticleCommentListParamsInterface } from '../../interfaces/port-params';

export abstract class ReadArticleCommentListPort implements
  PortInterface<ReadArticleCommentListParamsInterface, List<CommentInterface>> {
  abstract connect(params: ReadArticleCommentListParamsInterface): Promise<List<CommentInterface>>;
}