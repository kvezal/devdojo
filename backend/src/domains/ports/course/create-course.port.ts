import { CourseInterface } from '../../interfaces/entities-params';
import { CreateCourseParamsInterface } from '../../interfaces/port-params';
import { PortInterface } from '../../interfaces/core';

export abstract class CreateCoursePort implements PortInterface<CreateCourseParamsInterface, CourseInterface> {
  abstract connect(course: CreateCourseParamsInterface): Promise<CourseInterface>;
}