import { CourseInterface } from '../../interfaces/entities-params';
import { PortInterface } from '../../interfaces/core';

export abstract class UpdateCoursePort implements PortInterface<CourseInterface, void> {
  abstract connect(course: CourseInterface): Promise<void>;
}