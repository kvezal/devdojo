import { CourseInterface } from '../../interfaces/entities-params';
import { PortInterface } from '../../interfaces/core';

export abstract class ReadCoursePort implements PortInterface<string, CourseInterface> {
  abstract connect(courseId: string): Promise<CourseInterface>;
}