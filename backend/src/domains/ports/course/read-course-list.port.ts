import { Pagination, PortInterface } from '../../interfaces/core';
import { CourseListType } from '../../types';

export abstract class ReadCourseListPort implements PortInterface<Pagination, CourseListType> {
  abstract connect(params: Pagination): Promise<CourseListType>;
}