import { PortInterface } from '../../interfaces/core';

export abstract class DeleteCoursePort implements PortInterface<string, void> {
  abstract connect(courseId: string): Promise<void>;
}