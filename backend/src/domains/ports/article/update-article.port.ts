import { ArticleItemEntity } from '../../entities/article';
import { PortInterface } from '../../interfaces/core';

export abstract class UpdateArticlePort implements PortInterface<ArticleItemEntity, void> {
  abstract connect(params: ArticleItemEntity): Promise<void>;
}