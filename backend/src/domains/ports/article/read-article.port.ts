import { ArticleInterface } from '../../interfaces/entities-params';
import { PortInterface } from '../../interfaces/core';

export abstract class ReadArticlePort implements PortInterface<string, ArticleInterface> {
  abstract connect(params: string): Promise<ArticleInterface>;
}