import { ArticleItemEntity } from '../../entities/article';
import { PortInterface } from '../../interfaces/core';

export abstract class CreateArticlePort implements PortInterface<ArticleItemEntity, void> {
  abstract connect(params: ArticleItemEntity): Promise<void>;
}