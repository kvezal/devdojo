import { ArticleInfoListType } from '../../types';
import { PortInterface } from '../../interfaces/core';
import { ReadArticleInfoListParamsInterface } from '../../interfaces/port-params';

export abstract class ReadArticleInfoListPort implements
  PortInterface<ReadArticleInfoListParamsInterface, ArticleInfoListType>{
  abstract connect(params: ReadArticleInfoListParamsInterface): Promise<ArticleInfoListType>;
}