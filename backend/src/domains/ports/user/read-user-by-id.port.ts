import { PortInterface } from '../../interfaces/core';
import { UserInterface } from '../../interfaces/entities-params';

export abstract class ReadUserByIdPort implements PortInterface<string, UserInterface> {
  abstract connect(params: string): Promise<UserInterface>;
}