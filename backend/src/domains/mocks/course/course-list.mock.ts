import { CourseInterface } from '../../interfaces/entities-params';
import { List } from '../../interfaces/core';
import { COURSE_MOCK } from './course.mock';

export const COURSE_LIST_MOCK: List<CourseInterface> = {
  items: [COURSE_MOCK],
  totalQuantity: 51,
  pageItemQuantity: 10,
  page: 6,
}