import { CourseInterface } from '../../interfaces/entities-params';

export const COURSE_MOCK: CourseInterface = {
  id: 'd81798c1-bfbe-4ee5-bce8-6945f29da307',
  name: 'Some course name',
  text: 'Some course text',
  image: 'path/to/course-image.jpg',
}