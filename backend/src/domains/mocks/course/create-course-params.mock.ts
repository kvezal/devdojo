export const CREATE_COURSE_PARAMS_MOCK = {
  name: 'Some course name',
  text: 'Some course text',
  image: 'path/to/course-image.jpg',
};