import { ContentItemTypeEnum } from "../../enums/content-item-type.enum";

export const CONTENT_ITEM_HEADER_MOCK = {
  type: ContentItemTypeEnum.header,
  text: 'Some title',
  weight: 'bold',
  size: '24px',
  color: '#021691',
};