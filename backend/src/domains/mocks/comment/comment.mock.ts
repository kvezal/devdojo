import { CommentInterface } from '../../interfaces/entities-params';
import { USER_MOCK } from '../user/user.mock';

export const COMMENT_MOCK: CommentInterface = {
  id: '9048badc-919f-4b41-a718-9ba870b1ca63',
  articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
  authorId: USER_MOCK.id,
  text: 'user comment',
  date: '2023-05-13T11:34:11.912Z'
}