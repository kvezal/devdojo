import { CommentInterface } from '../../interfaces/entities-params';
import { USERS_MOCK } from '../user/users.mock';
import { List } from '../../interfaces/core';

export const COMMENT_LIST_PARAMS_MOCK: List<CommentInterface> = {
  items: [
    {
      id: '9048badc-919f-4b41-a718-9ba870b1ca63',
      articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
      authorId: USERS_MOCK[0].id,
      text: 'user comment 1',
      date: '2023-05-13T11:34:11.912Z'
    },
    {
      id: 'abae83d6-b6dd-4a5f-be00-4f389a1ad8a0',
      articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
      authorId: USERS_MOCK[1].id,
      text: 'user comment 2',
      date: '2023-05-13T12:34:11.912Z'
    },
    {
      id: '9048badc-919f-4b41-a718-9ba870b1ca63',
      articleId: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
      authorId: USERS_MOCK[0].id,
      text: 'user comment 3',
      date: '2023-05-13T12:44:11.912Z'
    }
  ],
  totalQuantity: 2,
  pageItemQuantity: 10,
  page: 1,
};