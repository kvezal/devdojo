import { ARTICLE_MOCK } from './article-params.mock';
import { ArticleInfoListType } from '../../types';

export const ARTICLE_INFO_LIST_PARAMS_MOCK: ArticleInfoListType = {
  items: [ARTICLE_MOCK],
  totalQuantity: 21,
  pageItemQuantity: 10,
  page: 3,
}