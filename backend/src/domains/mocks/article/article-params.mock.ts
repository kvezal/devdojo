import { CONTENT_ITEM_HEADER_MOCK } from '../content-item/content-item-header.mock';
import { ArticleInterface } from '../../interfaces/entities-params';

export const ARTICLE_MOCK: ArticleInterface = {
  id: '8ef45824-1918-4bf8-86b0-38f3e89cd22b',
  name: 'Article title',
  image: 'path/to/image',
  category: 'development',
  contentItems: [CONTENT_ITEM_HEADER_MOCK],
  authorId: '33d98b84-f9d8-4575-905b-9431cfc0165e',
  courseId: 'd81798c1-bfbe-4ee5-bce8-6945f29da307',
};