import { UserInterface } from '../../interfaces/entities-params';

export const USER_MOCK: UserInterface = {
  id: '33d98b84-f9d8-4575-905b-9431cfc0165e',
  name: 'Orpheus',
  image: '33d98b84-f9d8-4575-905b-9431cfc0165e.jpg',
  rating: 1000
};