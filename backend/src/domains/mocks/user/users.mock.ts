import { UserInterface } from '../../interfaces/entities-params';

export const USERS_MOCK: UserInterface[] = [
  {
    id: '33d98b84-f9d8-4575-905b-9431cfc0165e',
    name: 'Orpheus',
    image: '33d98b84-f9d8-4575-905b-9431cfc0165e.jpg',
    rating: 1000
  },
  {
    id: '68de1cfd-30fc-4616-be3b-f73e5ac14d42',
    name: 'Duncan',
    image: '68de1cfd-30fc-4616-be3b-f73e5ac14d42.jpg',
    rating: 150
  }
];