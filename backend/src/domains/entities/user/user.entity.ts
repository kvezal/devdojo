import { UserInterface } from '../../interfaces/entities-params';

export class UserEntity {
  static create(params: UserInterface): UserEntity {
    return new UserEntity(params);
  }

  public get id(): UserInterface['id'] {
    return this.params.id;
  }

  public get name(): UserInterface['name'] {
    return this.params.name;
  }

  public get image(): UserInterface['image'] {
    return this.params.image;
  }

  public get rating(): UserInterface['rating'] {
    return this.params.rating;
  }

  constructor(private readonly params: UserInterface) {}
}