import { USER_MOCK } from '../../mocks';
import { UserEntity } from './user.entity';

describe(UserEntity, () => {
  describe('id', () => {
    it('should contain correct value', () => {
      const userEntity = UserEntity.create(USER_MOCK);
      expect(userEntity.id).toEqual(USER_MOCK.id);
    });
  });

  describe('name', () => {
    it('should contain correct value', () => {
      const userEntity = UserEntity.create(USER_MOCK);
      expect(userEntity.name).toEqual(USER_MOCK.name);
    });
  });

  describe('image', () => {
    it('should contain correct value', () => {
      const userEntity = UserEntity.create(USER_MOCK);
      expect(userEntity.image).toEqual(USER_MOCK.image);
    });
  });

  describe('rating', () => {
    it('should contain correct value', () => {
      const userEntity = UserEntity.create(USER_MOCK);
      expect(userEntity.rating).toEqual(USER_MOCK.rating);
    });
  });
});