import { ArticleInfoItemEntity } from './article-info-item.entity';
import { ARTICLE_MOCK } from '../../../mocks';

describe(ArticleInfoItemEntity, () => {
  describe('id', () => {
    it('should contain correct value', () => {
      const result = ArticleInfoItemEntity.create(ARTICLE_MOCK);
      expect(result.id).toEqual(ARTICLE_MOCK.id);
    });
  });

  describe('name', () => {
    it('should contain correct value', () => {
      const result = ArticleInfoItemEntity.create(ARTICLE_MOCK);
      expect(result.name).toEqual(ARTICLE_MOCK.name);
    });
  });

  describe('image', () => {
    it('should contain correct value', () => {
      const result = ArticleInfoItemEntity.create(ARTICLE_MOCK);
      expect(result.image).toEqual(ARTICLE_MOCK.image);
    });
  });
});