import { ArticleInfoItemType } from '../../../types';

export class ArticleInfoItemEntity {
  static create(params: ArticleInfoItemType) {
    return new ArticleInfoItemEntity(params);
  }

  public get id(): ArticleInfoItemType['id'] {
    return this.params.id;
  }

  public get name(): ArticleInfoItemType['name'] {
    return this.params.name;
  }

  public get image(): ArticleInfoItemType['image'] {
    return this.params.image;
  }

  constructor(private readonly params: ArticleInfoItemType) {}
}