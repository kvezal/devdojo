import { ArticleItemEntity } from './article-item.entity';
import { ARTICLE_MOCK, USER_MOCK } from '../../../mocks';
import { ContentItemEntity } from '../../content-item/content-item.entity';
import { UserEntity } from '../../user';

describe(ArticleItemEntity, () => {
  describe('id', () => {
    it('should contain correct value', () => {
      const result = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      expect(result.id).toEqual(ARTICLE_MOCK.id);
    });
  });

  describe('name', () => {
    it('should contain correct value', () => {
      const result = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      expect(result.name).toEqual(ARTICLE_MOCK.name);
    });
  });

  describe('image', () => {
    it('should contain correct value', () => {
      const result = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      expect(result.image).toEqual(ARTICLE_MOCK.image);
    });
  });

  describe('category', () => {
    it('should contain correct value', () => {
      const result = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      expect(result.category).toEqual(ARTICLE_MOCK.category);
    });
  });

  describe('contentItems', () => {
    it('should contain correct value', () => {
      const expectResult = new ContentItemEntity(ARTICLE_MOCK.contentItems[0]);
      jest.spyOn(ContentItemEntity, 'create')
        .mockReturnValueOnce(expectResult);
      const result = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      expect(result.contentItems).toEqual([expectResult]);
    });
  });

  describe('author', () => {
    it('should contain correct value', () => {
      const expectResult = new UserEntity(USER_MOCK);
      jest.spyOn(UserEntity, 'create')
        .mockReturnValueOnce(expectResult);
      const result = ArticleItemEntity.create({
        article: ARTICLE_MOCK,
        author: USER_MOCK,
      });
      expect(result.author).toEqual(expectResult);
    });
  });
});