import { ArticleInterface, ContentItemParamsInterface, UserInterface } from '../../../interfaces/entities-params';
import { ArticleParamsInterface } from '../../../interfaces/entities-params/article-params.interface';
import { ContentItemEntity } from '../../content-item/content-item.entity';
import { UserEntity } from '../../user';

export class ArticleItemEntity {
  static create(params: ArticleParamsInterface): ArticleItemEntity {
    return new ArticleItemEntity(params);
  }

  public get id(): ArticleInterface['id'] {
    return this.params.article.id;
  }

  public get name(): ArticleInterface['name'] {
    return this.params.article.name;
  }

  public get image(): ArticleInterface['image'] {
    return this.params.article.image;
  }

  public get category(): ArticleInterface['category'] {
    return this.params.article.category;
  }

  public get contentItems(): ContentItemEntity[] {
    return this.#contentItems;
  }

  public get author(): UserEntity {
    return this.#author;
  }

  #contentItems: ContentItemEntity[];
  #author: UserEntity;

  constructor(private readonly params: ArticleParamsInterface) {
    this.#contentItems = this.mapContentItems(params.article.contentItems);
    this.#author = this.mapAuthor(params.author);
  }

  private mapContentItems(contentItems: ContentItemParamsInterface[]): ContentItemEntity[] {
    return contentItems.map((contentItem: ContentItemParamsInterface) => {
      return ContentItemEntity.create(contentItem);
    });
  }

  private mapAuthor(user: UserInterface): UserEntity {
    return UserEntity.create(user);
  }
}