import { ArticleInfoItemType, ArticleInfoListType } from '../../../types';
import { ArticleInfoItemEntity } from '../info-item/article-info-item.entity';

export class ArticleInfoListEntity {
  static create(params: ArticleInfoListType): ArticleInfoListEntity {
    return new ArticleInfoListEntity(params);
  }

  public get items(): ArticleInfoItemEntity[] {
    return this.#items;
  }

  public get totalQuantity(): ArticleInfoListType['totalQuantity'] {
    return this.params.totalQuantity;
  }

  public get pageItemQuantity(): ArticleInfoListType['pageItemQuantity'] {
    return this.params.pageItemQuantity;
  }

  public get page(): ArticleInfoListType['page'] {
    return this.params.page;
  }

  #items: ArticleInfoItemEntity[];

  constructor(private readonly params: ArticleInfoListType) {
    this.#items = this.mapItems(params.items);
  }

  private mapItems(items: ArticleInfoItemType[]): ArticleInfoItemEntity[] {
    return items.map((item: ArticleInfoItemEntity) => ArticleInfoItemEntity.create(item));
  }
}