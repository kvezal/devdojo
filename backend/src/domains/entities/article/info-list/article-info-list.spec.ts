import { ArticleInfoListEntity } from './article-info-list.entity';
import { ARTICLE_INFO_LIST_PARAMS_MOCK } from '../../../mocks/article/article-info-list-params.mock';
import { ArticleInfoItemEntity } from '../info-item/article-info-item.entity';

describe(ArticleInfoListEntity, () => {
  describe('items', () => {
    it('should be contain correct value', () => {
      const result = ArticleInfoListEntity.create(ARTICLE_INFO_LIST_PARAMS_MOCK);
      const expectResult = [
        new ArticleInfoItemEntity(
          ARTICLE_INFO_LIST_PARAMS_MOCK.items[0]
        )
      ];
      expect(result.items).toEqual(expectResult);
    });
  });

  describe('totalQuantity', () => {
    it('should be contain correct value', () => {
      const result = ArticleInfoListEntity.create(ARTICLE_INFO_LIST_PARAMS_MOCK);
      expect(result.totalQuantity).toEqual(ARTICLE_INFO_LIST_PARAMS_MOCK.totalQuantity);
    });
  });

  describe('pageItemQuantity', () => {
    it('should be contain correct value', () => {
      const result = ArticleInfoListEntity.create(ARTICLE_INFO_LIST_PARAMS_MOCK);
      expect(result.pageItemQuantity).toEqual(ARTICLE_INFO_LIST_PARAMS_MOCK.pageItemQuantity);
    });
  });

  describe('page', () => {
    it('should be contain correct value', () => {
      const result = ArticleInfoListEntity.create(ARTICLE_INFO_LIST_PARAMS_MOCK);
      expect(result.page).toEqual(ARTICLE_INFO_LIST_PARAMS_MOCK.page);
    });
  });
});