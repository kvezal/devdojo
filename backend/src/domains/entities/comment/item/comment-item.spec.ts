import {
  COMMENT_MOCK,
  USER_MOCK
} from '../../../mocks';
import { CommentItemEntity } from './comment-item.entity';
import { UserEntity } from '../../user';

describe(CommentItemEntity, () => {
  describe('id', () => {
    it('should contain correct value', () => {
      const userEntity = CommentItemEntity.create({
        comment: COMMENT_MOCK,
        user: USER_MOCK
      });
      expect(userEntity.id).toEqual(COMMENT_MOCK.id);
    });
  });

  describe('articleId', () => {
    it('should contain correct value', () => {
      const userEntity = CommentItemEntity.create({
        comment: COMMENT_MOCK,
        user: USER_MOCK
      });
      expect(userEntity.articleId).toEqual(COMMENT_MOCK.articleId);
    });
  });

  describe('author', () => {
    it('should be called create method of UserEntity', () => {
      const expectedResult = new UserEntity(USER_MOCK);
      const createSpy = jest.spyOn(UserEntity, 'create');
      const userEntity = CommentItemEntity.create({
        comment: COMMENT_MOCK,
        user: USER_MOCK
      });
      userEntity.author;
      expect(createSpy).toHaveBeenCalledWith(USER_MOCK);
    });
  });

  describe('text', () => {
    it('should contain correct value', () => {
      const userEntity = CommentItemEntity.create({
        comment: COMMENT_MOCK,
        user: USER_MOCK
      });
      expect(userEntity.text).toEqual(COMMENT_MOCK.text);
    });
  });

  describe('date', () => {
    it('should contain correct value', () => {
      const userEntity = CommentItemEntity.create({
        comment: COMMENT_MOCK,
        user: USER_MOCK
      });
      expect(userEntity.date).toEqual(COMMENT_MOCK.date);
    });
  });
});