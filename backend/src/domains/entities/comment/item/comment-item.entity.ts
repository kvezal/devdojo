import { CommentInterface, CommentParamsInterface, UserInterface } from '../../../interfaces/entities-params';
import { UserEntity } from '../../user';

export class CommentItemEntity {
  public static create(params: CommentParamsInterface): CommentItemEntity {
    return new CommentItemEntity(params);
  }

  public get id(): CommentInterface['id'] {
    return this.params.comment.id;
  }

  public get articleId(): CommentInterface['articleId'] {
    return this.params.comment.articleId;
  }

  public get author(): UserInterface {
    return UserEntity.create(this.params.user);
  }

  public get text(): CommentInterface['text'] {
    return this.params.comment.text;
  }

  public get date(): CommentInterface['date'] {
    return this.params.comment.date;
  }

  constructor(private readonly params: CommentParamsInterface) {}
}