import { CommentInterface, CommentListParamsInterface, UserInterface } from '../../../interfaces/entities-params';
import { ArrayConverterUtils } from '../../../utils';
import { CommentItemEntity } from '../item/comment-item.entity';
import { List } from '../../../interfaces/core';

export class CommentListEntity {
  static create(params: CommentListParamsInterface) {
    return new CommentListEntity(params);
  }

  public get items(): CommentItemEntity[] {
    return this.#items;
  }

  public get totalQuantity(): List<CommentInterface>['totalQuantity'] {
    return this.params.commentList.totalQuantity;
  }

  public get pageItemQuantity(): List<CommentInterface>['pageItemQuantity'] {
    return this.params.commentList.pageItemQuantity;
  }

  public get page(): List<CommentInterface>['page'] {
    return this.params.commentList.page;
  }

  #items: CommentItemEntity[];

  constructor(private readonly params: CommentListParamsInterface) {
    this.#items = this.mapItems({
      comments: params.commentList.items,
      users: params.users
    });
  }

  private mapItems(params: {
    comments: CommentInterface[];
    users: UserInterface[];
  }): CommentItemEntity[] {
    const userMap: Map<UserInterface['id'], UserInterface> = ArrayConverterUtils.convertToMapByProperty({
      items: params.users,
      property: 'id'
    });
    const commentMap: Map<CommentInterface['id'], CommentInterface> = ArrayConverterUtils.convertToMapByProperty({
      items: params.comments,
      property: 'id'
    });
    return [...commentMap.values()].map((comment: CommentInterface) => CommentItemEntity.create({
      comment,
      user: userMap.get(comment.authorId)
    }));
  }
}