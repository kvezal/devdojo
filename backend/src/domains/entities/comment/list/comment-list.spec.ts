import { CommentListEntity } from './comment-list.entity';
import { COMMENT_LIST_PARAMS_MOCK, USERS_MOCK } from '../../../mocks';
import { CommentItemEntity } from '../index';

describe(CommentListEntity, () => {
  describe('items', () => {
    it('should be called create method of CommentEntity', () => {
      const createSpy = jest.spyOn(CommentItemEntity, 'create');
      CommentListEntity.create({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK
      });
      expect(createSpy).toHaveBeenNthCalledWith(1, {
        comment: COMMENT_LIST_PARAMS_MOCK.items[2],
        user: USERS_MOCK[0],
      })
      expect(createSpy).toHaveBeenNthCalledWith(2, {
        comment: COMMENT_LIST_PARAMS_MOCK.items[1],
        user: USERS_MOCK[1],
      })
    });

    it('should contain correct result', () => {
      const expectedResult = [
        new CommentItemEntity({
          comment: COMMENT_LIST_PARAMS_MOCK.items[2],
          user: USERS_MOCK[0]
        }),
        new CommentItemEntity({
          comment: COMMENT_LIST_PARAMS_MOCK.items[1],
          user: USERS_MOCK[1]
        }),
      ];
      jest.spyOn(CommentItemEntity, 'create')
        .mockReturnValueOnce(expectedResult[0]);
      jest.spyOn(CommentItemEntity, 'create')
        .mockReturnValueOnce(expectedResult[1]);
      const result = CommentListEntity.create({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK
      });
      expect(result.items).toEqual(expectedResult);
    });
  });

  describe('totalQuantity', () => {
    it('should contain correct result', () => {
      const result = CommentListEntity.create({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK
      });
      expect(result.totalQuantity).toEqual(COMMENT_LIST_PARAMS_MOCK.totalQuantity);
    });
  });

  describe('pageItemQuantity', () => {
    it('should contain correct result', () => {
      const result = CommentListEntity.create({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK
      });
      expect(result.pageItemQuantity).toEqual(COMMENT_LIST_PARAMS_MOCK.pageItemQuantity);
    });
  });

  describe('page', () => {
    it('should contain correct result', () => {
      const result = CommentListEntity.create({
        commentList: COMMENT_LIST_PARAMS_MOCK,
        users: USERS_MOCK
      });
      expect(result.page).toEqual(COMMENT_LIST_PARAMS_MOCK.page);
    });
  });
});