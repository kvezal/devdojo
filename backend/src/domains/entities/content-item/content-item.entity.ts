import { ContentItemParamsInterface } from '../../interfaces/entities-params';
import { ContentItemTypeEnum } from '../../enums/content-item-type.enum';

export class ContentItemEntity {
  static create(params: ContentItemParamsInterface): ContentItemEntity {
    return new ContentItemEntity(params);
  }

  public get type(): ContentItemTypeEnum {
    return this.params.type;
  }

  public get text(): ContentItemParamsInterface['text'] {
    return this.params.text;
  }

  public get weight(): ContentItemParamsInterface['weight'] {
    return this.params.weight;
  }

  public get size(): ContentItemParamsInterface['size'] {
    return this.params.size;
  }

  public get color(): ContentItemParamsInterface['color'] {
    return this.params.color;
  }

  constructor(private readonly params: ContentItemParamsInterface) {}
}