import { ContentItemTypeEnum } from '../../enums/content-item-type.enum';
import { ContentItemEntity } from './content-item.entity';
import { CONTENT_ITEM_HEADER_MOCK } from '../../mocks'

describe(ContentItemEntity, () => {
  describe('type', () => {
    it('should contain correct value', () => {
      const result = ContentItemEntity.create(CONTENT_ITEM_HEADER_MOCK);
      expect(result.type).toEqual(CONTENT_ITEM_HEADER_MOCK.type);
    });
  });

  describe('text', () => {
    it('should contain correct value', () => {
      const result = ContentItemEntity.create(CONTENT_ITEM_HEADER_MOCK);
      expect(result.text).toEqual(CONTENT_ITEM_HEADER_MOCK.text);
    });
  });

  describe('weight', () => {
    it('should contain correct value', () => {
      const result = ContentItemEntity.create(CONTENT_ITEM_HEADER_MOCK);
      expect(result.weight).toEqual(CONTENT_ITEM_HEADER_MOCK.weight);
    });
  });

  describe('size', () => {
    it('should contain correct value', () => {
      const result = ContentItemEntity.create(CONTENT_ITEM_HEADER_MOCK);
      expect(result.size).toEqual(CONTENT_ITEM_HEADER_MOCK.size);
    });
  });

  describe('color', () => {
    it('should contain correct value', () => {
      const result = ContentItemEntity.create(CONTENT_ITEM_HEADER_MOCK);
      expect(result.color).toEqual(CONTENT_ITEM_HEADER_MOCK.color);
    });
  });
});