import { CourseItemEntity } from '../index';
import { CourseListEntity } from './course-list.entity';
import { COURSE_LIST_MOCK } from '../../../mocks';

describe(CourseListEntity, () => {
  describe('items', () => {
    it('should be called create method of CourseEntity', () => {
      const createSpy = jest.spyOn(CourseItemEntity, 'create');
      CourseListEntity.create(COURSE_LIST_MOCK);
      expect(createSpy).toHaveBeenCalledWith(COURSE_LIST_MOCK.items[0]);
    });

    it('should contain result CourseEntity\'s create method', () => {
      const expectedResult = [ CourseItemEntity.create(COURSE_LIST_MOCK.items[0]) ];
      jest.spyOn(CourseItemEntity, 'create').mockReturnValueOnce(expectedResult[0]);
      const result = CourseListEntity.create(COURSE_LIST_MOCK);
      expect(result.items).toEqual(expectedResult);
    });
  });

  describe('totalQuantity', () => {
    it('should contain correct value', () => {
      const result = CourseListEntity.create(COURSE_LIST_MOCK);
      expect(result.totalQuantity).toEqual(COURSE_LIST_MOCK.totalQuantity);
    });
  });

  describe('pageItemQuantity', () => {
    it('should contain correct value', () => {
      const result = CourseListEntity.create(COURSE_LIST_MOCK);
      expect(result.pageItemQuantity).toEqual(COURSE_LIST_MOCK.pageItemQuantity);
    });
  });

  describe('page', () => {
    it('should contain correct value', () => {
      const result = CourseListEntity.create(COURSE_LIST_MOCK);
      expect(result.page).toEqual(COURSE_LIST_MOCK.page);
    });
  });
});