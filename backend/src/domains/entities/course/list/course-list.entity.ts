import { CourseInterface } from '../../../interfaces/entities-params';
import { CourseItemEntity } from '../index';
import { CourseListType } from '../../../types';

export class CourseListEntity {
  static create(params: CourseListType): CourseListEntity {
    return new CourseListEntity(params);
  }

  public get items(): CourseItemEntity[] {
    return this.#items;
  }

  public get totalQuantity(): number {
    return this.params.totalQuantity;
  }

  public get page(): number {
    return this.params.page;
  }

  public get pageItemQuantity(): number {
    return this.params.pageItemQuantity;
  }

  #items: CourseItemEntity[];

  constructor(private readonly params: CourseListType) {
    this.#items = this.mapItems(params.items);
  }

  private mapItems(items: CourseInterface[]): CourseItemEntity[] {
    return items.map((item: CourseInterface) => CourseItemEntity.create(item));
  }
}