import { CourseItemEntity } from './course-item.entity';
import { COURSE_MOCK } from '../../../mocks';

describe(CourseItemEntity, () => {
  describe('id', () => {
    it('should contain correct value', () => {
      const result = CourseItemEntity.create(COURSE_MOCK);
      expect(result.id).toEqual(COURSE_MOCK.id);
    });
  });

  describe('name', () => {
    it('should contain correct value', () => {
      const result = CourseItemEntity.create(COURSE_MOCK);
      expect(result.name).toEqual(COURSE_MOCK.name);
    });
  });

  describe('text', () => {
    it('should contain correct value', () => {
      const result = CourseItemEntity.create(COURSE_MOCK);
      expect(result.text).toEqual(COURSE_MOCK.text);
    });
  });

  describe('image', () => {
    it('should contain correct value', () => {
      const result = CourseItemEntity.create(COURSE_MOCK);
      expect(result.image).toEqual(COURSE_MOCK.image);
    });
  });
});