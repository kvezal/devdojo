import { CourseInterface } from '../../../interfaces/entities-params';

export class CourseItemEntity {
  static create(params: CourseInterface): CourseItemEntity {
    return new CourseItemEntity(params);
  }

  public get id(): CourseInterface['id'] {
    return this.params.id;
  }

  public get name(): CourseInterface['name'] {
    return this.params.name;
  }

  public get text(): CourseInterface['text'] {
    return this.params.text;
  }

  public get image(): CourseInterface['image'] {
    return this.params.image;
  }

  constructor(private readonly params: CourseInterface) {}
}