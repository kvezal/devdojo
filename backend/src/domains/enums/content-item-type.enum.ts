export enum ContentItemTypeEnum {
  header = 'header',
  text = 'text',
  image = 'image',
}